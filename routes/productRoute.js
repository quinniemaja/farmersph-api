const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const orderController = require('../controllers/OrderController');
const auth = require('../auth');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
	if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
		cb(null, true);
	}else cb(null, false);
}

const upload = multer({
	storage: storage, 
	limits:{
		fileSize:1024 * 1024 * 5
	},
	fileFilter: fileFilter
});

router.post('/register', upload.single('productImage'),(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == true) {
		productController.registerProduct(req.body, req.file).then(resultFromController => res.send(resultFromController))
	} else {
		console.log('not authorized')
		return false
	}
})

router.get('/', (req, res) => {
	productController.showAllProducts().then(resultFromController => res.send(resultFromController))
})

router.get('/:id', (req, res) => {
	productController.showSpecificProduct(req.params).then(resultFromController => res.send(resultFromController))
})

router.post('/archive/:id', auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == true) {
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))	
	} else console.log('not authorized'); return false
})

router.patch('/update/:id', (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == true) {
	productController.updateProduct(req.body, req.params).then(resultFromController => res.send(resultFromController))	
	} else console.log('not authorized'); return false
})

router.post('/rate-product/:id', (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.commentProduct(req.body, req.params).then(resultFromController => res.send(resultFromController))
})


module.exports = router
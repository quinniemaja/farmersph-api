const Order = require('../models/orderModel');
const auth = require('../auth');
const bcrypt = require('bcrypt');


module.exports.allOrders = () => {

	return Order.find({}).then(orders=> {
		console.log(orders)
			return orders	
	});
}

module.exports.addToCart = (orderId, reqBody) => {
	return Order.findById(orderId).then(result => {
		console.log(result)
		if(result){
			Object.assign(result, reqBody).save()
			return result
		}else return false
	}
	)

}

module.exports.deleteSpecificOrder = (orderId) => {
	return Order.findByIdAndDelete(orderId).then(result => {
		console.log(result)
		return result
	})

}

const Product = require('../models/productModel');
const auth = require('../auth');
const bcrypt = require('bcrypt');

// Register Product
exports.registerProduct = (productDetails, imagePath) => {
	console.log(imagePath)
	const { name, description, type, price, numberOfStocks, availableStock } = productDetails;
	
	const newProduct = new Product({
		name: name, 
		description: description,
		type: type,
		price: price,
		stocked: numberOfStocks,
		available: availableStock,
		productImage: imagePath.path
	})

	return newProduct.save().then((product, err) => {
		if(err) return false
		else return product
	})
}; 

// Show Available Product
exports.showAllProducts = () => {
	return Product.find({}).then(product => {
		return product
	})
};

// Show specific product
exports.showSpecificProduct = (productId) => {
	const {id} = productId;
	return Product.findById(id).then(product => {
		if(product) return product 
		else return false
	})
};

// Archive Product 
exports.archiveProduct = (productId, reqBody) => {
	const { id } = productId;
	return Product.findById(id).then(product => {
		console.log(product)
		product.isActive = reqBody.isActive
		return product.save().then((prod, err) => {
			if(err){ console.log(err); return false}
			else return product
		})
	})	
}

// Update Product
exports.updateProduct = (productDetailChange, productId) => {
		const { name, description, type, price, numberOfStocks, availableStock } = productDetailChange;
	const {id} = productId;
		let updatedProduct = {

		name: name, 
		description: description,
		type: type,
		price: price,
		stocked: numberOfStocks,
		available: availableStock
	}
	return Product.findByIdAndUpdate(id, updatedProduct).then(product => {
		if(product){
			console.log(product)
			return product;
		}else return false;
	})
}

// Rate Product 
exports.commentProduct = async(data, productId) => {
	const userData = auth.decode(req.headers.authorization)
	let isProductCommented = await Product.findById(productId.id).then(product => {
		product.comment.push(
			{
				userId: userData.id,
				rate: data.rate,
				title: data.title,
				description: data.description
			}
		)

		return product.save().then((product, err) => {
			if(err) return false
			else return product 
		})
	})

	if(isProductCommented) return true 
	else return false
}


/* https://gentle-dusk-18521.herokuapp.com/ | https://git.heroku.com/gentle-dusk-18521.git */
// require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 4000;
const userRoute = require('./routes/userRoute');
const productRoute = require('./routes/productRoute');


app.use('/uploads',express.static('uploads'))
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors())

const connect = mongoose.connection;
mongoose.connect('mongodb+srv://admin:admin131@bootcamp.y9mz6.mongodb.net/farmersph?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});
connect.on('error', console.error.bind(console, 'ERROR: Failed to Connect to the Database'));
connect.once('open', () => { console.log('Successfully Connected to the Database')});

app.use('/user', userRoute);
app.use('/product', productRoute);


app.listen(port, () => console.log(`Server is running at port: ${port}.`))


// quinnie's repo : https://safe-bastion-71965.herokuapp.com/ | https://git.heroku.com/safe-bastion-71965.git